import { withPluginApi } from "discourse/lib/plugin-api";

function initializeUser(api) {
  const currentUser = api.getCurrentUser();
  const pathnameArr = [];
  api.onPageChange(() => {
    let curUrl = window.location.pathname;
    pathnameArr.push(curUrl);
    let prevUrl = pathnameArr[pathnameArr.length-2];
    if(prevUrl) {
      if (!~prevUrl.indexOf("/support") && ~curUrl.indexOf("/support") || ~prevUrl.indexOf("/support") && !~curUrl.indexOf("/support")) {
        location.reload();
      }
    }

  });
  if (currentUser) {
    if (currentUser.admin || currentUser.moderator) {
           Discourse.ExternalNavItem = Discourse.NavItem.extend({
             href : function() {
               return this.get('href');
             }.property('href')
           });

           I18n.translations.en.js.filters.support = { title: "Support", help: "support" };

           Discourse.NavItem.reopenClass({
             buildList : function(category, args) {
               var list = this._super(category, args);
               list.push(Discourse.ExternalNavItem.create({href: '/support', name: 'support'}));
               return list;
             }
           });

      // Discourse.SiteSettings.top_menu = Discourse.SiteSettings.top_menu + "|support"
    }
  }
}

export default {
  name: 'add-link',
  initialize() {
    withPluginApi("0.8.7", initializeUser);
  }
};
