# name: discourse-wf
# version: 0.1

#register_custom_html(extraNavItem: "<li><a href='/support'>Support WF</a></li>")

# Discourse.filters.push(:support)
#Discourse.anonymous_filters.push(:support)

after_initialize do
  load File.expand_path('../app/controllers/sup_controller.rb', __FILE__)
  # require_dependency 'topic_query'
  # class ::TopicQuery
  #   def list_support(excluded_topic_ids = [])
  #     create_list(:support, {})
  #   end
  # end
  #
  # require_dependency 'list_controller'
  # class ::ListController
  #   def support_feed
  #     list = generate_list_for("support", target_user, list_opts)
  #     respond_with_list(list)
  #   end
  # end

  Discourse::Application.routes.append do
    get "/support" => "sup#index"
  end

end
